import React, {Component} from 'react';
import {
  Linking,
  View,
  Text,
  TextInput,
  StyleSheet,
  Button,
  TouchableOpacity,
  Alert,
  ImageBackground,
  Image,
} from 'react-native';



export default class App extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground
          resizeMode="cover"
          style={{flex: 1}}
          source={{
            uri: 'https://www.nawpic.com/media/2020/summer-aesthetic-wallpaper-iphone-nawpic-13.jpg',
          }}>
          <View style={gaya.pembungkus}>
            <Image
              source={{uri:'https://www.clipartmax.com/png/full/4-40722_logo-logo-coconut.png'}}
              style={gaya.logo}
            />
            <Text style={{fontSize: 20, fontWeight: '900', color: 'black'}}>
              Masuk ke Cocoanut
            </Text>
            <View style={{flexDirection: 'row'}}></View>
            <TextInput
              style={gaya.Kotak}
              placeholder="Nama Pengguna.."
              placeholderTextColor={'white'}
            />
            <TextInput
              style={gaya.Kotak}
              placeholder="Kata Sandi.."
              placeholderTextColor={'white'}
              secureTextEntry={true}
            />
            <Button
              title="Login"
               onPress={() =>{ this.props.navigation.navigate("Cari Film")}}/>
            <Text
              style={{fontSize: 15, color: 'white', marginTop: 10}}
              onPress={() => Linking.openURL('https://coconuts.co/register/')}>
              Daftar?
            </Text>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const gaya = StyleSheet.create({
  logo: {
    marginTop: -100,
    marginBottom: 10,
    width: 250,
    height: 250,

  },
  pembungkus: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Kotak: {
    width: 300,
    height: 50,
    padding: 10,
    backgroundColor: 'grey',
    marginVertical: 10,
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 1,
  },
});
