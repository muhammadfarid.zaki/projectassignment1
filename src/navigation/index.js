import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import CariFilm from '../screens/Cari Film'
import InformasiDetail from'../screens/Informasi Detail'
import LoginPage from '../screens/LoginPage'
const Stack = createNativeStackNavigator();



const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen name="Login Page" component={LoginPage} options={{headerShown: false}}/>
        <Stack.Screen name="Cari Film" component={CariFilm} options={{headerShown: false}}/>
        <Stack.Screen name="Informasi Detail Film" component={InformasiDetail}/>
        </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;